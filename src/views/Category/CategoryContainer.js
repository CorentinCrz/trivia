import React, {Component, createRef} from 'react';
import Api from '../../helpers/Api';
import Local from '../../helpers/Local';
import Category from './Category';

class CategoryContainer extends Component {
    state = {
        categories: []
    }

    //async is needed because we are calling an api
    async componentDidMount(){
        const categories = await Api.getCategories();
        //Check if the currentQuestion is full, meaning that some categories were already selected
        //and all the corresponding question were answered.
        //In that case we don't want to display the first selected categories.
        if (Local.get('currentQuestion') === 'full'){
            for (let i = 0; i < categories.length; i++) {
                //category in localStorage is an array of previously selected categories
                for (let a = 0; a < Local.get('category').length; a++) {
                    if (Local.get('category')[a] === '' + categories[i].id){
                        categories.splice(i, 1);
                    }
                }
            }
        }
        this.setState({
            categories: categories
        });
    }

    // createRef in order to bring back select value to its parent
    select = createRef();

    handleSubmit = (e) => {
        // here I prevent de fault bh of submitting form
        e.preventDefault();
        // set ref value in localstorage
        Local.push('category', this.select.current.value);
        //remove currentQuestion is full and tell parent to display another component
        Local.remove('currentQuestion');
        this.props.renderComponent();
    }

    render(){
        return (
            <Category
                categories={this.state.categories}
                handleSubmit={this.handleSubmit}
                select={this.select}
            />
        );
    }
}

export default CategoryContainer;