import React from 'react';
import PropTypes from 'prop-types';

//display some form with a select where you can choose your category
const Category = ({ categories, handleSubmit, select }) => (
    <section>
        {categories.length > 0 && (
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="exampleFormControlSelect1">Please select a category</label>
                    <select className="form-control" id="exampleFormControlSelect1" ref={select}>
                        {categories.map(category => (
                            <option value={category.id} key={category.id}>{category.title}</option>
                        ))}
                    </select>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        )}
    </section>
);

// validation
Category.propTypes = {
    categories: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
            clues_count: PropTypes.number
        }),
    ),
}

export default Category;