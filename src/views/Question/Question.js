import React from 'react';
import PropTypes from 'prop-types';
import Capitalize from '../../components/CapitalizeText/CapitalizeText'

//no need to use a class for such a small component
const Question = ({ category, currentQuestionIndex, handleSubmit, answerInput }) => {
    const currentQuestion = category.clues[currentQuestionIndex];
    return (
        <section>
            <h3>
                {/*catitalize the first letter of the category*/}
                <Capitalize text={category.title}/> :
            </h3>
            {/*display the question*/}
            <p className="question__title">
                {currentQuestion.question}
            </p>
            {/*this form is used to send the data the the parent component*/}
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <input className="form-control" ref={answerInput} />
                </div>
                <button className="btn btn-primary" type="submit">
                    Next
                </button>
            </form>
        </section>
    );
}

// validation
Question.propTypes = {
    category: PropTypes.shape({}).isRequired,
    currentQuestionIndex: PropTypes.number.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    answerInput: PropTypes.shape({
        value: PropTypes.instanceOf(HTMLInputElement)
    }),
};

export default Question;