import React, { Component, createRef } from 'react';
import Api from '../../helpers/Api';
import Local from '../../helpers/Local';
import Question from './Question';

class QuestionContainer extends Component {
    state = {
        category: null,
    };

    /**
     * Format a string
     *      - Remove spaces
     *      - Remove slashes
     *      - Replace special characters to "normal" character (é to e, ...)
     *
     * @param string
     * @returns {string}
     */
    formatString = (string) => {
        return string.toLowerCase().replace(/\s/g,'').replace(/\\/g, '').normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    };

    // createRef in order to bring back input value to its parent
    answerInput = createRef();

    //setState depending on localStorage before component mount
    componentWillMount(){
        if (!Local.get('currentQuestion')) {
            Local.set('currentQuestion', 0);
        }
        this.setState({
            currentQuestion: Local.get('currentQuestion')
        });
    }

    // async needed when using an api
    async componentDidMount() {
        const categories = Local.get('category');
        const data = await Api.getCategoryById(categories[categories.length - 1]);
        // stored response in the state;
        this.setState({
            category: data
        });
    }

    //method call on form sumbit
    handleSubmit = (e) => {
        // here I prevent default bh of submitting form
        e.preventDefault();
        // get the ref value
        let answer = this.formatString(this.answerInput.current.value);

        // If no value is set, alert the user
        if (answer) {
            let currentQuestion = this.formatString(this.state.category.clues[this.state.currentQuestion].answer); // Get the answer of the question

            // test correct answer
            if (answer === currentQuestion){
                //correct answer
                Local.add('score');
                //check if score is greater than 10
                if (Local.get('score') >= 10) {
                    this.props.displayMessage('T’es un winner');
                } else {
                    //test if there is an other question
                    if (Local.get('currentQuestion') < this.state.category.clues.length - 1) {
                        Local.add('currentQuestion');
                    } else {
                        //if not call the parent to render another component
                        Local.set('currentQuestion', 'full');
                        this.props.renderComponent();
                    }
                    this.setState({
                        currentQuestion: Local.get('currentQuestion'),
                    });
                    //reset input value
                    this.answerInput.current.value = '';
                }
            } else {
                //wrong answer
                Local.add('life', -1)
                if (Local.get('life') <= 0) {
                    this.props.displayMessage('You lose');
                }
            }
            //update the score in the parent component
            this.props.updateScore();
        } else {
            alert('You have to enter a value.');
        }
    }

    render() {
        const { category, currentQuestion } = this.state;
        // at first render, category will be null so we need to wait
        // before using data.
        if (!category) return <div>is loading</div>

        return (
            <div>
                <Question
                    category={category}
                    currentQuestionIndex={currentQuestion}
                    handleSubmit={this.handleSubmit}
                    answerInput={this.answerInput} // plug createRef to chidlren
                />
            </div>

        );
    }
}

export default QuestionContainer;