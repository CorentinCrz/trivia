import React from 'react';

//this class is used to capitalize the first letter of a text
class CapitalizedText extends React.Component {

    render() {
        return (
            jsUcfirst(this.props.text)
        );
    }
};
function jsUcfirst(string)
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
export default CapitalizedText;