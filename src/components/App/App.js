import React, { Component } from 'react';
import CategoryContainer from '../../views/Category/CategoryContainer';
import './App.css';
import QuestionContainer from "../../views/Question/QuestionContainer";
import Local from '../../helpers/Local';
import logo from './logo.svg';
import life from './life.png';

class App extends Component {
    constructor(props) {
        super(props)

        // Bind the this context to the functions
        this.renderComponent = this.renderComponent.bind(this);
        this.displayMessage = this.displayMessage.bind(this);
        this.updateScore = this.updateScore.bind(this);
        this.restart = this.restart.bind(this);
    }

    //setState depending on localStorage before component mount
    componentWillMount(){
        this.renderComponent()
        this.updateScore()
    }

    // update score in state depending on localStorage
    updateScore(){
        //set score to 0 if it doesn't exist in localStorage
        if (!Local.get('score')) {
            Local.set('score', 0);
        }
        if (!Local.get('life')) {
            Local.set('life', 3);
        }
        this.setState({
            score: Local.get('score'),
            life: Local.get('life')
        });
    }

    //this method render either CategoryContainer or QuestionContainer depending on localStorage
    //CategoryContainer is used to choose some category
    //QuestionContainer is used to answer some questions
    renderComponent() {
        //if category isn't set or currentQuestion is full meaning all questions have been answers for this category
        if (Local.get('category') === null || Local.get('currentQuestion') === "full"){
            this.setState({
                toRender: <CategoryContainer renderComponent={this.renderComponent}/>
            });
        } else {
            this.setState({
                toRender: <QuestionContainer renderComponent={this.renderComponent} displayMessage={this.displayMessage} updateScore={this.updateScore}/>
            });
        }
    }

    //display win or lose message
    displayMessage(message) {
        this.setState({
            toRender: <h3 className="end-message">{message}</h3>
        });
        Local.clear();
    }

    //restart the game on the begining state
    restart(){
        Local.clear()
        this.renderComponent()
        this.updateScore()
    }

    //this is call during the render method an display life logos depending on localStorage
    displayLife(){
        let lifes = []

        //add some css class if there is only one life left
        let buttonClass = "footer-life"
        buttonClass += this.state.life <=1 ? " last-life" : ""

        for (let i = 0; i < this.state.life; i++) {
            lifes.push(<img src={life} key={i} className={buttonClass} alt="life logo" />)
        }

        return lifes
    }

    render() {
        //add some css class if the score is equal to 7 or more
        let buttonClass = "btn btn-sm float-right";
        buttonClass += this.state.score > 6 ? " btn-success" : " btn-light";
        return (
            <div className="App">
                {/*display header with title and reset button*/}
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1>The Great Quiz</h1>
                    <button className="btn btn-danger" onClick={this.restart}>Restart</button>
                </header>

                <section className="wrap">
                    {this.state.toRender}
                </section>

                {/*display footer with score and life counter*/}
                <footer className="App-footer">
                    <div>
                        <button className={buttonClass} disabled>Score {this.state.score}</button>
                    </div>
                    <div>
                        {this.displayLife()}
                    </div>
                </footer>
            </div>
        );
    }
}

export default App;
