//used to call api
class api {
    /**
     * Set the URL of the API
     */
    constructor() {
        this.url = 'http://jservice.io/api'; // Url of the API
    }

    /**
     * Get categories from the API
     *
     * @param count amount of categories to return, limited to 100 at a time
     * @returns {Promise<any>}
     */
    async getCategories(count = 100) {
        const response = await fetch(`${this.url}/categories?count=${count}`);
        const json = await response.json();

        return json;
    }

    /**
     * Get category by ID
     *
     * @param id id of the category
     * @returns {Promise<any>}
     */
    async getCategoryById(id) {
        const response = await fetch(`${this.url}/category?id=${id}`);
        const json = await response.json();

        return json;
    }
}

export default new api();