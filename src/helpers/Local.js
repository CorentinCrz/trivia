//this class is used to store data in localstorage
class local {
    set(item, value) {
        localStorage.setItem(item, JSON.stringify(value));
    }
    get(item) {
        return JSON.parse(localStorage.getItem(item));
    }
    remove(item) {
        localStorage.removeItem(item);
    }
    clear(item) {
        localStorage.clear();
    }
    //must be number
    add(item, value = 1) {
        localStorage.setItem(item, parseInt(localStorage.getItem(item)) + parseInt(value));
    }
    //must be json array
    push(item, value) {
        let array = this.get(item);
        if (!Array.isArray(array)){
            array = [];
        }
        array.push(value);
        this.set(item, array);
    }
}

export default new local();